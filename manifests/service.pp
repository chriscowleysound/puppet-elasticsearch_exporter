# elasticsearch_exporter::service
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include elasticsearch_exporter::service
class elasticsearch_exporter::service {
  include ::systemd
  file {'/etc/systemd/system/elasticsearch_exporter.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('elasticsearch_exporter/elasticsearch_exporter.systemd.epp', {
      'bin_dir' => $::elasticsearch_exporter::install_dir
      }
    )
  }
  ~>Exec['systemctl-daemon-reload']
  ->service {'elasticsearch_exporter':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }
}

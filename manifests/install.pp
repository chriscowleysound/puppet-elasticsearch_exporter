# elasticsearch_exporter::install
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include elasticsearch_exporter::install
class elasticsearch_exporter::install {
  include ::archive

  if $elasticsearch_exporter::manage_user {
    ensure_resource('user', ['elasticsearch_exporter'], {
      ensure => 'present',
      system => true,
    })
  }
  if $elasticsearch_exporter::manage_group {
    ensure_resource('group', ['elasticsearch_exporter'], {
      ensure => 'present',
      system => true,
    })
    Group['elasticsearch_exporter']->User['elasticsearch_exporter']
  }
  archive { "/tmp/${elasticsearch_exporter::local_archive_name}":
    ensure          => present,
    extract         => true,
    extract_path    => '/opt',
    source          => $elasticsearch_exporter::real_archive_url,
    checksum_verify => false,
    creates         => "${::elasticsearch_exporter::install_dir}/elasticsearch_exporter",
    cleanup         => true,
  }

}

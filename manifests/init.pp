# elasticsearch_exporter
#
# Main class, includes all other classes
#
# @version [Optional[String]] Which version to install
# @manage_user [Optional[Boolean]] Whether to create the elasticsearch_exporter user. Must be created by other means if set to false
# @manage_group [Optional[Boolean]] Whether to create the elasticsearch_exporter group. Must be created by other means if set to false
# @archive_name [Optional[String]] URL of archive to download. 
#   include elasticsearch_exporter
class elasticsearch_exporter (
  $version = $::elasticsearch_exporter::params::version,
  $manage_user = $::elasticsearch_exporter::params::manage_user,
  $manage_group = $::elasticsearch_exporter::params::manage_group,
  $archive_name = $::elasticsearch_exporter::params::archive_name,
  $real_archive_url = $::elasticsearch_exporter::params::real_archive_url,
  $local_archive_name = $::elasticsearch_exporter::params::local_archive_name,

) inherits elasticsearch_exporter::params {
  class { '::elasticsearch_exporter::install': }
  ->class{ '::elasticsearch_exporter::config': }
  ~>class{ '::elasticsearch_exporter::service': }
  ->Class['::elasticsearch_exporter']
}

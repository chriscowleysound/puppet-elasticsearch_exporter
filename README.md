
# elasticsearch_exporter

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with elasticsearch_exporter](#setup)
    * [What elasticsearch_exporter affects](#what-elasticsearch_exporter-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with elasticsearch_exporter](#beginning-with-elasticsearch_exporter)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the Prometheus Elasticsearch Exporter.

## Setup

### What elasticsearch_exporter affects **OPTIONAL**

Installs the release from Github and creates a Systemd unit file to run the service under a dedicated user.


### Beginning with elasticsearch_exporter  

```
node '<elasticsearch-node>' {
  include ::elasticsearch_exporter
}
```

This will monitor the Elasticsearch on `localhost:9200`  which should be fine for and sane system. For now there are no parameters to control the configuration as I do not need them.

## Limitations

Tested on Centos 7, but should be fine on any Linux that uses Systemd.

## Development

Create a Merge Request
